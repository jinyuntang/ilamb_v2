#!/usr/bin/env python
"""
Computes a multimodel mean.
"""
import os,sys,argparse
from ILAMB.ModelResult import ModelResult
from ILAMB.Variable import Variable
import ILAMB.ilamblib as il
from ILAMB.constants import bnd_months
from netCDF4 import Dataset
import numpy as np

def InitializeModels(model_root,models=[],verbose=False,filter="",model_year=[]):
    """Initializes a list of models

    Initializes a list of models where each model is the subdirectory
    beneath the given model root directory. The global list of models
    will exist on each processor.

    Parameters
    ----------
    model_root : str
        the directory whose subdirectories will become the model results
    models : list of str, optional
        only initialize a model whose name is in this list
    verbose : bool, optional
        enable to print information to the screen
    model_year : 2-tuple, optional
        shift model years from the first to the second part of the tuple

    Returns
    -------
    M : list of ILAMB.ModelResults.ModelsResults
       a list of the model results, sorted alphabetically by name

    """
    # initialize the models
    M = []
    if len(model_year) != 2: model_year = None
    max_model_name_len = 0
    if verbose: print("\nSearching for model results in %s\n" % model_root)
    for subdir, dirs, files in os.walk(model_root):
        for mname in dirs:
            if len(models) > 0 and mname not in models: continue
            M.append(ModelResult(os.path.join(subdir,mname), modelname = mname, filter=filter, model_year = model_year))
            max_model_name_len = max(max_model_name_len,len(mname))
        break
    M = sorted(M,key=lambda m: m.name.upper())

    # optionally output models which were found
    if verbose:
        for m in M:
            print(("    {0:>45}").format(m.name))

    if len(M) == 0:
        if verbose: print("No model results found")
        sys.exit(0)
        
    return M

            
parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--model_root', dest="model_root", metavar='root', type=str, nargs=1, default=["./"],
                    help='root at which to search for models')
parser.add_argument('--models', dest="models", metavar='m', type=str, nargs='+',default=[],
                    help='specify which models to run, list model names with no quotes and only separated by a space.')
parser.add_argument('--filter', dest="filter", metavar='filter', type=str, nargs=1, default=[""],
                    help='a string which much be in the model filenames')
parser.add_argument('-q','--quiet', dest="quiet", action="store_true",
                    help='enable to silence screen output')

# setup
args = parser.parse_args()
M  = InitializeModels(args.model_root[0],args.models,not args.quiet,filter=args.filter[0])
Vs = []
for m in M:
    Vs += [v for v in m.variables.keys() if v not in Vs]


skip = ['time', 'time_bnds', 'lat', 'lat_bnds', 'lon', 'lon_bnds', 'plev', 'time_bounds', 'type', 'nav_lat', 'nav_lon', 'bounds_nav_lon', 'bounds_nav_lat', 'area', 'areacello']
Vs = [v for v in Vs if v not in skip]

# compute mean
yrs = (np.asarray(range(1850,2016),dtype=float)-1850)*365
yrs = (yrs[:,np.newaxis] + bnd_months[:12]).flatten()
t_bnd = np.asarray([yrs[:-1],yrs[1:]]).T
t = t_bnd.mean(axis=1)
lat_bnd,lon_bnd,lat,lon = il.GlobalLatLonGrid(1.0)
lat_bnd = np.asarray([lat_bnd[:-1],lat_bnd[1:]]).T
lon_bnd = np.asarray([lon_bnd[:-1],lon_bnd[1:]]).T
for v in Vs:
    print("checking ",v)
    data  = np.zeros((t.size,lat.size,lon.size))
    count = np.zeros((t.size,lat.size,lon.size),dtype=int)
    skip = False
    unit = None
    for i,m in enumerate(M):
        print("  ",m.name)
        if skip: continue
        try:
            u = m.extractTimeSeries(v)
        except:
            continue
        if not (u.temporal and u.spatial):
            skip = True
            continue
        if u.layered or u.data.ndim > 3:
            skip = True
            continue
        if unit is None: unit = u.unit
        print("    convert")
        u.convert(unit)
        print("    interpolate %e %e" % (u.data.min(),u.data.max()))
        ui = u.interpolate(time=t,lat=lat,lon=lon,lat_bnds=lat_bnd,lon_bnds=lon_bnd)
        print("    sum")
        with np.errstate(under='ignore',over='ignore'):
            data  +=  ui.data.data*(ui.data.mask==False)
            count += (ui.data.mask==False)
    if count.sum() == 0: continue
    print("  average")
    with np.errstate(all='ignore'):
        data = np.ma.masked_array(data=(data/count.clip(1)),mask=(count==0))
    print("  output")
    with Dataset("%s_mean.nc" % v,mode="w") as dset:
        o = Variable(data       = data,
                     unit       = unit,
                     name       = v,
                     time       = t,
                     time_bnds  = t_bnd,
                     lat        = lat,
                     lat_bnds   = lat_bnd,
                     lon        = lon,
                     lon_bnds   = lon_bnd)
        o.toNetCDF4(dset)
